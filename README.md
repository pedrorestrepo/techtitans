SENG 513 - Tech Titans
Daniel Sheedy, Pedro Restrepo, Sebastian De La Rosa, Subhodeep Raychaudhuri, Tyler Tripathy
===========================================================================================

To run the project, you must install Meteor JS.

You can install meteor JS by following these instructions:

https://www.meteor.com/install

In order to run the server, navigate to the techtitans project directory and run meteor:
```
cd techtitans
meteor
```

The server will start running on http://localhost:3000/. Navigating to this URL will redirect you to the landing page.

If you wish to reset the database, you may run:

```
meteor reset
```
