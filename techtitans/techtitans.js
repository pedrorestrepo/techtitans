if (Meteor.isClient) {

  Router.route('/', function () {
  // render the Home template with a custom data context
  this.render('trending');
  Router.go('/trending');

  });

  Router.route('/login');
  Router.route('/registration');
  Router.route('/new');
  Router.route('/trending');
  Router.route('/manage');

  Template.login.events({
    'click .signup': function(){
        Router.go('/registration');
    },
    'submit form': function(event) {
      if (!event.isDefaultPrevented()) {
           event.preventDefault();
           var usernameVar = event.target.username.value;
           var passwordVar = event.target.password.value;
           Meteor.loginWithPassword(usernameVar, passwordVar, function(error){
             if(error){
                $("#incorrect").removeClass("hidden");
                $("#incorrectDiv").addClass("addMargin");
                $("#uname").focus();
             } else{
        				Router.go('/new')
             }
           });
       }
       else{
          event.preventDefault();
       }
     }
	});

  Template.registration.events({
    'click .back': function(){
        Router.go('/login');
    },
    'submit form': function(event) {
      if (!event.isDefaultPrevented()) {
           event.preventDefault();
           var usernameVar = event.target.username.value;
           var passwordVar = event.target.password.value;
           var retypePwVar = event.target.retypePw.value;
           var emailVar = event.target.email.value;
           var topicsVar = $(".chosen-select").val()
           if (passwordVar === retypePwVar){
             Accounts.createUser({
             username: usernameVar,
             password: passwordVar,
             email: emailVar,
             profile: {
               topics: topicsVar
             }

            });
            Meteor.logout();
            Router.go('/login');
           }
         }
         else{
            event.preventDefault();
         }
       }
	});

  Template.manage.events({
    'click .back': function(){
        Router.go('/new');
    },
    'submit form': function(event) {
      if (!event.isDefaultPrevented()) {
           event.preventDefault();
           var usernameVar = event.target.username.value;
           var emailVar = event.target.email.value;
           var newPWVar = event.target.newPW.value;
           var retypeNewPwVar = event.target.retypeNewPw.value;
           var topicsVar = $(".chosen-select").val()

           Meteor.users.update({_id: Meteor.userId()}, {$set: {'username': usernameVar}});
           Meteor.users.update({_id: Meteor.userId()}, {$set: {'email': emailVar}});
           Meteor.users.update({_id: Meteor.userId()}, {$set: {'profile.topics': topicsVar}});

           if (newPWVar === "" || retypeNewPwVar === ""){
             Router.go('/new');

           }
           else{
             Accounts.resetPassword(Meteor.userId(), newPWVar, {logout: false});
             Router.go('/new');

           }
         }
         else{
            event.preventDefault();
         }
       }
	});

  Template.manage.helpers({
    'getTopics': function(){
      if(Meteor.user()){
        var userTopics = Meteor.user().profile.topics;
        return userTopics;
      }},
      'androidSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Android"){
                return true;
            }
          }
        }
      },
      'appleSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Apple"){
                return true;
            }
          }
        }
      },
      'gadgetsSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Gadgets"){
                return true;
            }
          }
        }
      },
      'innovationSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Innovation"){
                return true;
            }
          }
        }
      },
      'linuxSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Linux"){
                return true;
            }
          }
        }
      },
      'programmingSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Programming"){
                return true;
            }
          }
        }
      },
      'windowsSelected': function(){
        if(Meteor.user()){
          var userTopics = Meteor.user().profile.topics;
          for (i=0; i<userTopics.length; i++){
            if (userTopics[i] === "Windows"){
                return true;
            }
          }
        }
      },
  });


  Template.navItems_loggedIn.helpers({
    activeIfTemplateIs: function (template) {
      var currentRoute = Router.current();
      var abc = currentRoute.lookupTemplate();
      return currentRoute &&
        template.toUpperCase()  === currentRoute.lookupTemplate().toUpperCase() ? 'active' : '';
    }

	});

  Template.navItems_loggedIn.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Router.go('/login');


    }
	});

	Template.trending.helpers({
		'getTrending': function(){
			return ArticlesList_Trending.find().fetch();
		}

	})

	Template.new.helpers({
    'getNew': function(){
			if(Meteor.user()){
				var topicslength = Meteor.user({_id: Meteor.userId()}).profile.topics.length;
				var UserData = Meteor.user().profile.topics;
				var returnval = ArticlesList_New.find({Topic: UserData[0]}).fetch();
				var listofarticles = [];
				for(var i = 1; i < topicslength; i ++){
					listofarticles = ArticlesList_New.find({Topic: UserData[i]}).fetch();
					returnval = returnval.concat(listofarticles);
				}
				return returnval;
			}
		}
	})

}

ArticlesList_New = new Mongo.Collection('articles_new');
ArticlesList_Trending = new Mongo.Collection('articles_trending');

if (Meteor.isServer) {
	Meteor.startup(function () {

  });

    if (ArticlesList_Trending.find().count() === 0) {

      ArticlesList_Trending.insert({
        Topic: "Apple",
        Source: "Engadget",
        External_Link: "http://www.engadget.com/2016/03/31/ipad-pro-9-7-review/",
        Article_Title: "iPad Pro 9.7 review: Apple's best tablet, but it won't replace a laptop",
        Article_Body: "Calling it a full-blown laptop replacement is a stretch, but Apple's new 9.7-inch iPad Pro is the best conventional tablet the company has made. It combines the power of the original 12.9-inch iPad Pro with the thin frame of the iPad Air 2. Additionally, it brings a vibrant, color-accurate screen and the best camera we've seen on an iPad to date. If you need a proper laptop, you should still buy one, but if all you want is a great tablet, the 9.7-inch Pro is the one to get.",
        Picture: "http://o.aolcdn.com/dims5/amp:81535e351555b479871e4f77f382bcb6a36957d9/t:1335,600/q:80/?url=http%3A%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fc162118b9c8c3f0598c54d60bec7ac96%2F203616393%2F97pro-fb3.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Apple",
        Source: "Engadget",
        External_Link: "http://www.engadget.com/2016/03/29/iphone-se-review/",
        Article_Title: "Apple iPhone SE review: A compelling blend of old and new",
        Article_Body: "Just because the iPhone SE looks like an old device doesn't mean it acts like one. Apple squeezed flagship-level performance and the best battery life we've seen from an iPhone in years into a familiar (and relatively inexpensive) frame. The phone lacks some of the 6s's niceties and its screen is just too small for some, but the iPhone SE is the best compact phone Apple has ever made.",
        Picture: "http://o.aolcdn.com/hss/storage/midas/bd65dbe530a21d8d8f4eba277a67bc54/203605735/se-fb1.jpg"
      });

      ArticlesList_Trending.insert({
        Topic: "Apple",
        Source: "9to5Mac",
        External_Link: "http://9to5mac.com/2016/04/01/apple-hangs-pirate-flag-over-infinite-loop-on-its-40th-birthday-2/",
        Article_Title: "Apple hangs pirate flag over Infinite Loop HQ on its 40th birthday",
        Article_Body: "In homage to its history, Apple has hung a pirate flag at its Infinite Loop Headquarters. On April 1st, 40 years ago, Apple was founded by Steve Jobs, Steve Wozniak and Ronald Wayne. The iconic flag, complete with rainbow Apple logo for an eye, dates back to the creation of the original Mac. The Mac team hung the flag as an act of rebellion, distinguishing their cool work, led by Jobs, from the rest of the company which was preoccupied making the Lisa.",
        Picture: "https://9to5mac.files.wordpress.com/2016/04/ce6r45cuaaahylssss.jpg?w=2000&h=0#038;h=401"
      });

      ArticlesList_Trending.insert({
        Topic: "Android",
        Source: "The Verge",
        External_Link: "http://www.theverge.com/2016/3/29/11323884/google-photos-android-undo-editing-non-destructive",
        Article_Title: "Google Photos for Android just made tweaking your photos painless",
        Article_Body: "An update to the Android version of Google Photos was released yesterday that adds an important feature from the iOS version of the app: non-destructive editing. The updated Android app will now track and save any individual edits you make to a photo, letting you go back — at any point — to adjust those edits or undo them completely.",
        Picture: "http://www.technobuffalo.com/wp-content/uploads/2016/03/Google-Photos-non-destructive-editing-768x432.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Android",
        Source: "TechCrunch",
        External_Link: "http://techcrunch.com/2016/03/28/samsung-pay-just-launched-in-china-but-already-faces-fierce-competition/",
        Article_Title: "Samsung Pay just launched in China, but already faces fierce competition",
        Article_Body: "Samsung Pay is now available in China, about six weeks after competitor Apple Pay launched there. Samsung Pay is partnered with China UnionPay, the bankcard association that until recently held a monopoly on electronic payments in China. It still processes the vast majority of the country’s card transactions, however, and claims 260 million users. Samsung Pay will initially support cards issued from nine Chinese banks before adding six more banks. In China, Samsung Pay is currently available on the maker’s flagship models—the Galaxy S7, Galaxy S7 Edge, Galaxy S6 Edge+, and Galaxy Note5—but may be supported by “additional mid-range models in the future,” the company said in a press release.",
        Picture: "https://tctechcrunch2011.files.wordpress.com/2016/03/shutterstock_237645046.jpg?w=738"
      });

      ArticlesList_Trending.insert({
        Topic: "Innovation",
        Source: "Fast Company",
        External_Link: "http://www.fastcompany.com/3058535/most-creative-people/teslas-35000-model-3-will-arrive-in-2017",
        Article_Title: "Tesla reveals its $35,000 car for the masses",
        Article_Body: "During a packed event at its design studio in Hawthorne, California on Thursday night, and in front of a huge global audience watching via livestream, Tesla CEO Elon Musk unveiled the Model 3, a $35,000 sedan capable of traveling at least 215 miles on a single charge. The vehicle isn’t expected to ship until late next year; however, you can preorder one now with a $1,000 down payment.",
        Picture: "http://b.fastcompany.net/multisite_files/fastcompany/imagecache/slideshow_large/slideshow/2016/04/3058535-slide-s-0-gallery-4.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Innovation",
        Source: "The Big Story",
        External_Link: "http://bigstory.ap.org/article/f356421d0fd2445dad5efd3d395611ae/new-virtual-reality-app-timelooper-takes-you-back-history",
        Article_Title: "New virtual reality app Timelooper takes you back in history",
        Article_Body: "Imagine watching frantic shopkeepers busily extinguish the Great Fire of London, or sheltering from Nazi bombing raids during the Blitz.Now, thanks to a new virtual reality app, you can travel back in time to be immersed in these events.The Timelooper app allows users to experience key moments in  history with just a smartphone and a cardboard headset. Timelooper plans to launch in New York City this April, allowing tourists to witness the famous kiss that was photographed in Times Square in August 1945 on VJ Day, the day World War II officially ended with the surrender of Japan, and to see the iconic picture of workers eating lunch atop a skyscraper during construction of the Rockefeller Center in 1932.",
        Picture: "http://cdn.phys.org/newman/csz/news/800/2016/newvirtualre.jpg"
      });

      ArticlesList_Trending.insert({
        Topic: "Linux",
        Source: "ZDNet",
        External_Link: "http://www.zdnet.com/article/microsoft-and-canonical-partner-to-bring-ubuntu-to-windows-10/",
        Article_Title: "​Microsoft and Canonical partner to bring Ubuntu to Windows 10",
        Article_Body: "According to sources at Canonical, Ubuntu Linux's parent company, and Microsoft, you'll soon be able to run Ubuntu on Windows 10. This will be more than just running the Bash shell on Windows 10. After all, thanks to programs such as Cygwin or MSYS utilities, hardcore Unix users have long been able to run the popular Bash command line interface (CLI) on Windows. With this new addition, Ubuntu users will be able to run Ubuntu simultaneously with Windows. This will not be in a virtual machine, but as an integrated part of Windows 10.",
        Picture: "https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/41/57/ms_loves_linux.png"
      });
      ArticlesList_Trending.insert({
        Topic: "Linux",
        Source: "ZDNet",
        External_Link: "http://www.zdnet.com/article/new-linux-tablet-aims-to-make-ubuntu-desktop-mobile/",
        Article_Title: "New Linux tablet aims to make Ubuntu desktop mobile",
        Article_Body: "Ubuntu fans thirsty for a tablet running the Linux OS may have a new option on the horizon from US outfit MJ Technology, which is touting four models in a new crowdfunding campaign on Indiegogo. If MJ Technology reaches its $200,000 target, the company promises to deliver 10.1-inch Mini Tanto for $230 by August. The device is advertised as having a 1.92GHz Intel Atom processor, 2GB, 64GB storage, and HD display at 1,920 x 1,200 pixels. The company says it has prototype tablets and is in final design testing phases but needs the extra cash to start production. Its goal is to build a true x86/x64 tablet, so that the device can run Ubuntu desktop, as opposed to BQ's ARM-based device, which can only run Ubuntu Touch.",
        Picture: "http://zdnet4.cbsistatic.com/hub/i/r/2016/04/01/3fc976bf-07e9-4cb6-bdb8-1a84d3c134e2/resize/770xauto/d23350e03449c9c43f63fff202ed9325/1002cut1024x1024.png"
      });
      ArticlesList_Trending.insert({
        Topic: "Gadgets",
        Source: "Reviewed",
        External_Link: "http://smarthome.reviewed.com/features/everything-that-works-with-amazon-echo-alexa",
        Article_Title: "Everything That Works With Amazon Echo and Alexa",
        Article_Body: "The Amazon Echo has quietly become the hottest smart home product on the market. Suddenly, every tech company wants to integrate its products with Amazon's customizable virtual assistant, Alexa.The integrations keep coming, at such a pace that it can be hard to know exactly which products work with Alexa. Amazon's website tries to keep on top of things, but its interface can be tough to navigate. It also (understandably) underplays the fact that you don't need an Echo to take advantage of Alexa and all the devices she can control.",
        Picture: "http://a4.images.reviewed.com/image/fetch/s--j3W1cbeu--/c_fill,cs_srgb,f_auto,fl_progressive.strip_profile,g_center,q_jpegmini,w_747/https://reviewed-production.s3.amazonaws.com/1459280559000/amazon-echo-alexa-dot-roundup-hero.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Gadgets",
        Source: "Tech Insider",
        External_Link: "http://www.techinsider.io/samsung-folding-smartphone-tablet-hybrid-2016-4",
        Article_Title: "Samsung's new folding smartphone",
        Article_Body: "The foldable smartphone that Samsung's been working on for the last few years could become a reality soon. According to Korean news site ETNews, Samsung will apparently start mass-producing the ",
        Picture: "http://static4.businessinsider.com/image/56fea050dd089524178b45cd-999-749/30ca98bd6410f29239ded71319f0ce83.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Gadgets",
        Source: "Pocket-lint",
        External_Link: "http://www.pocket-lint.com/news/137191-nasa-is-making-an-exhibit-that-ll-let-you-roam-mars-using-microsoft-hololens",
        Article_Title: "NASA is making an exhibit that'll let you roam Mars using Microsoft HoloLens",
        Article_Body: "You'll be able to explore Mars in a few months, as long as you're near Florida. Shortly after Microsoft ended its main keynote at Build 2016, NASA announced that it teamed up with the Windows 10-maker to let you take a virtual tour of Mars using Microsoft's HoloLens augmented reality headset. The US space agency has come up with a new exhibition called Destination: Mars. It will open at the Kennedy Space Center Visitor Complex in Florida this summer, allowing you to ",
        Picture: "http://cdn.pocket-lint.com/r/s/970x/assets/images/phpatmppb.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Programming",
        Source: "Info-World",
        External_Link: "http://www.infoworld.com/article/3050747/javascript/google-predicts-angular-2-is-not-too-far-away.html",
        Article_Title: "Google predicts Angular 2 is 'not too far away'",
        Article_Body: "The general release of Angular 2.0, the much-anticipated follow-up to the first major version of Google’s popular JavaScript framework, is “not too far away,” a Google official said. “We’ll probably [offer] a release candidate next month and then fix a bunch of bugs in the following month,” Brad Green, Google engineering director, said Thursday afternoon at Microsoft’s Build conference in San Francisco. A release candidate is the last stage before a general release. A highlight of the release is improved rendering. “Already in Angular 2, we’re dramatically faster than Angular 1 in our rendering capabilities,” Green said. Right now, Angular 2 runs 2.5 times faster out of the box than Angular 1; the goal is to be five times faster. To provide an upgrade path, the ng-upgrade capability enables mixing of Angular 2 components and services into an existing Angular 1 application.",
        Picture: "http://images.techhive.com/images/article/2015/03/female-exec7-100572009-primary.idge.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Programming",
        Source: "ArsTechnica",
        External_Link: "http://arstechnica.com/information-technology/2016/03/microsofts-new-ai-tools-help-developers-build-smart-apps-and-bots/",
        Article_Title: "Microsoft’s new AI tools help developers build smart apps and bots",
        Article_Body: "Microsoft is offering new tools to help developers build interactive bots that understand natural language, the company announced at its Build conference today. There are two key components, which are available in preview and are both part of the larger Cortana Intelligence Suite. ",
        Picture: "http://cdn.arstechnica.net/wp-content/uploads/2016/03/microsoft-bot-framework-640x342.png"
      });
      ArticlesList_Trending.insert({
        Topic: "Programming",
        Source: "Info-World",
        External_Link: "http://www.infoworld.com/article/3046421/javascript/minimalist-javascript-framework-feathers-20-ready-to-fly.html",
        Article_Title: "Microsoft’s new AI tools help developers build smart apps and bots",
        Article_Body: "Feathers 2.0, positioned as a “minimalist” JavaScript framework, will be launched as a platform for building real-time apps and APIs. Composed of a few hundred lines of code, Feathers helps developers transition from monolithic application development to microservices. It serves as a wrapper over the Express framework, the Socket.IO real-time engine, and the Primus real-time framework. Services in Feathers provide create, read, update, and delete capabilities via methods such as find, get, create, update, patch, and remove. RESTful and real-time APIs are exposed over HTTP/HTTPS via WebSockets. A real-time API can be built with four commands, according to Feathers documentation.",
        Picture: "http://images.techhive.com/images/article/2015/03/swan_feather_pond-100574715-primary.idge.jpg"
      });

      ArticlesList_Trending.insert({
        Topic: "Windows",
        Source: "Bitbag",
        External_Link: "http://www.thebitbag.com/windows-10-newest-updates-biometrics-cross-device/143936",
        Article_Title: "Windows 10 Newest Updates: Biometrics, Cross-Device And More",
        Article_Body: "Microsoft announced Windows 10 Anniversary update at its Build 2016 event in San Francisco which brings a slate of new features for the company’s operating system. Windows chief Terry Myerson stated that the Redstone update will be coming this summer, for free to all Windows 10 users. The update is coming to Xbox One too, turning any retail Xbox One into a dev unit and bringing Windows 10 apps and Cortana to Xbox One. The update includes key innovations such as biometrics security, Linux command line, Windows Ink and a far more powerful Cortana.",
        Picture: "http://images.dailytech.com/nimage/Windows_10_Cross_Platform_Wide.jpg"
      });
      ArticlesList_Trending.insert({
        Topic: "Windows",
        Source: "Venturebeat",
        External_Link: "http://venturebeat.com/2016/03/25/game-developers-weigh-in-on-the-openness-of-microsofts-windows-10-platform/",
        Article_Title: "Game developers weigh in on the openness of Microsoft’s Windows 10 platform",
        Article_Body: "Tim Sweeney is one of the best-known technical experts in the game industry and the CEO of Gears of War creator Epic Games. He kicked off a big debate over the openness of Windows earlier this month when he wrote an opinion piece that warned about Microsoft’s moves with Windows 10 to close off its Windows app store and compromise the openness of the Windows PC. Sweeney expanded upon that theme with a technical description of what it will require for Windows to remain an open platform. In response, Microsoft’s Kevin Gallo, the corporate VP for Windows, said, “The Universal Windows Platform is a fully open ecosystem, available to every developer, that can be supported by any store. We continue to make improvements for developers; for example, in the Windows 10 November Update, we enabled people to easily side-load apps by default, with no UX required.”",
        Picture: "http://1u88jj3r4db2x4txp44yqfj1.wpengine.netdna-cdn.com/wp-content/uploads/2015/10/tim-sweeney-2-930x587.jpg"
      });
    }

  if (ArticlesList_New.find().count() === 0) {

    ArticlesList_New.insert({
      Topic: "Apple",
      Source: "The Guardian",
      External_Link: "http://www.theguardian.com/technology/2016/apr/05/iphone-6s-plus-security-hole-lockscreen-flaw-passcode-twitter",
      Article_Title: "iPhone 6S security hole lets attackers access contacts and photos without passcode",
      Article_Body: "A security flaw with the iPhone 6S and 6S Plus will let anyone bypass the phone lock and access personal information without having to know the passcode. The bug, discovered by Jose Rodriguez, who found a similar security hole last year, requires Siri but unlike many other iPhone hacks is relatively easy to execute. All an attacker needs to do is fire up Siri from the lock screen and prompt it to search Twitter for any email address. Once one is found, 3D Touching the email address will bring up a context menu from which you can create a new contact or add to an existing contact.",
      Picture: "https://i.guim.co.uk/img/media/06c291de38d654b0a49efcf52c9a57e5f37edcae/457_289_2586_1551/master/2586.jpg?w=620&q=55&auto=format&usm=12&fit=max&s=518079734954059f2e32605823a31556"
    });


    ArticlesList_New.insert({
      Topic: "Android",
      Source: "TechCrunch",
      External_Link: "http://techcrunch.com/2016/04/05/android-auto-launches-in-18-new-countries-including-brazil-india-and-russia/",
      Article_Title: "Android Auto launches in 18 new countries, including Brazil, India and Russia",
      Article_Body: "Android Auto, Google’s in-car operating system, is now available in eighteen new countries, including Brazil and India. This is Android Auto’s biggest international roll out so far and increases the number of countries it is available in from 11 to 29. Three of them—Brazil, India, and Russia—are among the fastest growing auto markets in the world (though of course not all cars purchased there will be compatible with Android Auto). Android Auto, which lets users connect their smartphones and control apps from their cars’ screens, launched in late 2014. Its main rival is Apple CarPlay.",
      Picture: "https://tctechcrunch2011.files.wordpress.com/2016/04/screen-shot-2016-04-05-at-3-28-43-pm.png?w=738"

    });
    ArticlesList_New.insert({
      Topic: "Android",
      Source: "MobileSyrup",
      External_Link: "http://mobilesyrup.com/2016/03/29/htc-says-upcoming-flagship-smartphone-is-the-fastest-and-smoothest-android-available/",
      Article_Title: "HTC says upcoming flagship smartphone is ‘the fastest and smoothest Android’ device available",
      Article_Body: "HTC has been teasing its upcoming smartphone for several weeks, specifically touting it to include a world class front and rear camera. Today, the company boasted about the device's power and showed off a glimpse of its final design through its official Twitter account. Images that leaked earlier this week reveal a design that's similar to what HTC teased on social media. HTC will officially unveil the HTC 10 on April 12th via a live stream on the company's official website.",
      Picture: "http://cdn.mobilesyrup.com/wp-content/uploads/2015/03/htconem9review-03490.jpg"

    });

    ArticlesList_New.insert({
      Topic: "Innovation",
      Source: "Extreme Tech",
      External_Link: "http://www.extremetech.com/extreme/225707-ibms-resistive-computing-could-massively-accelerate-ai-and-get-us-closer-to-asimovs-positronic-brain",
      Article_Title: "IBM’s resistive computing could massively accelerate AI — and get us closer to Asimov’s Positronic Brain",
      Article_Body: "With the recent rapid advances in machine learning has come a renaissance for neural networks — computer software that solves problems a little bit like a human brain, by employing a complex process of pattern-matching distributed across many virtual nodes, or “neurons.” Modern compute power has enabled neural networks to recognize images, speech, and faces, as well as to pilot self-driving cars, and win at Go and Jeopardy. Most computer scientists think that is only the beginning of what will ultimately be possible.",
      Picture: "http://www.extremetech.com/wp-content/uploads/2015/10/AI-640x353.jpg"
    });

    ArticlesList_New.insert({
      Topic: "Innovation",
      Source: "The Week",
      External_Link: "http://theweek.com/articles/615707/innovation-week-virtual-reality-headset-that-diagnose-concussions",
      Article_Title: "Innovation of the week: A virtual reality headset that can diagnose concussions",
      Article_Body: "Virtual reality could soon help doctors quickly and accurately diagnose concussions, said Daniel Terdiman at Fast Company. The FDA recently approved a virtual reality headset called Eye-Sync that checks the wearer for abnormal eye movement, a telltale sign someone has suffered a concussion. The patient looks into the device, which then records and analyzes eye movements to make a diagnosis ",
      Picture: "http://api.theweek.com/sites/default/files/styles/large/public/0408_Tech2.jpg?itok=S5wiY6FG"
    });


    ArticlesList_New.insert({
      Topic: "Linux",
      Source: "Softpedia",
      External_Link: "http://news.softpedia.com/news/chakra-gnu-linux-gets-major-qt-5-6-update-kde-plasma-5-6-1-and-frameworks-5-20-502521.shtml",
      Article_Title: "Chakra GNU/Linux Gets Major Qt 5.6 Update, KDE Plasma 5.6.1 and Frameworks 5.20",
      Article_Body: "Earlier today, April 3, 2016, Chakra GNU/Linux maintainer Neofytos Kolokotronis proudly announced the availability of some major updates in the main software repositories of the Arch Linux inspired operating system. This quick update that followed the first 5.6 release provides several bug fixes to Plasma users, in addition to the many changes that were introduced in 5.6.0 which aimed at enhancing users' experience.",
      Picture: "http://i1-news.softpedia-static.com/images/news2/chakra-gnu-linux-gets-major-qt-5-6-update-kde-plasma-5-6-1-and-frameworks-5-20-502521-2.jpg"
    });

    ArticlesList_New.insert({
      Topic: "Gadgets",
      Source: "Reviewed",
      External_Link: "http://smarthome.reviewed.com/features/everything-that-works-with-amazon-echo-alexa",
      Article_Title: "Everything That Works With Amazon Echo and Alexa",
      Article_Body: "The Amazon Echo has quietly become the hottest smart home product on the market. Suddenly, every tech company wants to integrate its products with Amazon's customizable virtual assistant, Alexa.The integrations keep coming, at such a pace that it can be hard to know exactly which products work with Alexa. Amazon's website tries to keep on top of things, but its interface can be tough to navigate. It also (understandably) underplays the fact that you don't need an Echo to take advantage of Alexa and all the devices she can control.",
      Picture: "http://a4.images.reviewed.com/image/fetch/s--j3W1cbeu--/c_fill,cs_srgb,f_auto,fl_progressive.strip_profile,g_center,q_jpegmini,w_747/https://reviewed-production.s3.amazonaws.com/1459280559000/amazon-echo-alexa-dot-roundup-hero.jpg"
    });


    ArticlesList_New.insert({
      Topic: "Gadgets",
      Source: "TechCrunch",
      External_Link: "http://techcrunch.com/2016/03/30/teardown-of-oculus-rift-finds-good-design-thats-somehow-relatively-easy-to-repair/",
      Article_Title: "Teardown of Oculus Rift finds good design that’s somehow relatively easy to repair",
      Article_Body: "When a new category of gadget appears, it’s understandable when the vanguard devices aren’t the most well designed, or are resistant to disassembly and repair. Fortunately, that doesn’t seem to be the case with the first production VR headset from Oculus — in its teardown, iFixit found the device to be an elegant piece of engineering that doesn’t punish the user with exotic screw types or tamper-evident seals.",
      Picture: "https://tctechcrunch2011.files.wordpress.com/2016/03/ugcbovvfomgrskko.jpg?w=738"
    });

    ArticlesList_New.insert({
      Topic: "Programming",
      Source: "Info-World",
      External_Link: "http://www.infoworld.com/article/3050747/javascript/google-predicts-angular-2-is-not-too-far-away.html",
      Article_Title: "Google predicts Angular 2 is 'not too far away'",
      Article_Body: "The general release of Angular 2.0, the much-anticipated follow-up to the first major version of Google’s popular JavaScript framework, is “not too far away,” a Google official said. “We’ll probably [offer] a release candidate next month and then fix a bunch of bugs in the following month,” Brad Green, Google engineering director, said Thursday afternoon at Microsoft’s Build conference in San Francisco. A release candidate is the last stage before a general release. A highlight of the release is improved rendering. “Already in Angular 2, we’re dramatically faster than Angular 1 in our rendering capabilities,” Green said. Right now, Angular 2 runs 2.5 times faster out of the box than Angular 1; the goal is to be five times faster. To provide an upgrade path, the ng-upgrade capability enables mixing of Angular 2 components and services into an existing Angular 1 application.",
      Picture: "http://images.techhive.com/images/article/2015/03/female-exec7-100572009-primary.idge.jpg"
    });

    ArticlesList_New.insert({
      Topic: "Programming",
      Source: "Phys.org",
      External_Link: "http://phys.org/news/2016-03-language-cells.html",
      Article_Title: "A programming language for living cells",
      Article_Body: "MIT biological engineers have created a programming language that allows them to rapidly design complex, DNA-encoded circuits that give new functions to living cells.Using this language, anyone can write a program for the function they want, such as detecting and responding to certain environmental conditions. They can then generate a DNA sequence that will achieve it.",
      Picture: "http://cdn.phys.org/newman/csz/news/800/2016/aprogramming.jpg"
    });


    ArticlesList_New.insert({
      Topic: "Windows",
      Source: "NeuroGadget",
      External_Link: "http://neurogadget.net/2016/04/03/new-features-windows-10-offer-lumia-smartphones/27404",
      Article_Title: "What New Features Does Windows 10 Offer Lumia Smartphones?",
      Article_Body: "The desktop version made its presence felt much earlier. The launch was made a year after Microsoft announced its plans to release the operating system. The all new Windows 10 platform has quite a few new and distinct features. A couple of such features include Windows Hello and Continuum. Many of the existing apps such as Snap Assist, Mail, and Calendar have also received interesting updates that make them work better than before.",
      Picture: "http://neurogadget.net/wp-content/uploads/2016/04/Windows-10-Redstone-Preview-14295-1-681x457.jpg"
    });
  }
}
